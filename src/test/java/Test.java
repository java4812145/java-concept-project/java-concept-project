import configuration.TestBase;
import jdk.jfr.Description;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Test extends TestBase {
    public WebDriver driver;

    @BeforeEach
    public void setUp() {
        driver = connectionDriver();
        driver.get("https://www.saucedemo.com/");
    }

    @AfterEach
    public void closeDriver(){
        closeDriver(driver);
    }
    @org.junit.jupiter.api.Test
    @Description("Acessar o site do Sauce Demo com sucesso")
    public void acessarSauceDemo() {
        String titulo = driver.findElement(By.className("login_logo")).getText();
        System.out.println(titulo);
        Assertions.assertEquals("Swag Labs", titulo);
    }

    @org.junit.jupiter.api.Test
    @Description("Realizar login com sucesso")
    public void fazerLogin() {
        String titulo = driver.getTitle();
        driver.findElement(By.id("user-name")).sendKeys("standard_user");
        driver.findElement(By.id("password")).sendKeys("secret_sauce");
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Assertions.assertEquals("Swag Labs", titulo);
    }
}
