package configuration;

import org.openqa.selenium.WebDriver;

public class TestBase extends WebConnection{
    public WebDriver driver;
    public WebDriver connectionDriver(){
        driver =  getDriver();
        return driver;
    }

    public void closeDriver(WebDriver driver){
        driver.quit();
    }
}
