package configuration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebConnection {
    private WebDriver driver;

    public WebDriver getDriver() {
        driver = new ChromeDriver();
        return driver;
    }

    public void tearDown() {
        this.driver.quit();
    }
}
